
#include <Arduino.h>
#include <Keyboard.h>

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels

#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

void mute(void) {
  display.clearDisplay();

  display.setTextSize(4);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(16,3);
  display.println(F("MUTE"));

  display.invertDisplay(true);
  display.display();
}

void on_air(void) {
  display.clearDisplay();

  display.setTextSize(3);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(8,3);
  display.println(F("ON AIR"));

  display.invertDisplay(false);
  display.display();
}

#define SWITCH_PIN 4
#include <Bounce2.h>

Bounce pushbutton = Bounce(SWITCH_PIN, 10);  // 10 ms debounce

void sendToggleSoundCommand() {
    Keyboard.press(KEY_LEFT_SHIFT);
    Keyboard.press(KEY_LEFT_GUI);
    Keyboard.press(KEY_F1);
    
    delay(10);
    Keyboard.releaseAll();
    Serial.println("Command sent");
}

void setup() {
    Serial.begin(9600);
    Keyboard.begin();

    display.setRotation(2);
    if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
      Serial.println(F("SSD1306 allocation failed"));
      for(;;); // Don't proceed, loop forever
    }
    display.display();

    pinMode(SWITCH_PIN, INPUT_PULLUP);
}

void loop() {
    if (pushbutton.update()) {
        if (pushbutton.fallingEdge()) {
            sendToggleSoundCommand();
        }
    } 

    int incomingByte = 0;
    if (Serial.available() > 0) {
        incomingByte = Serial.read();
        char read = char(incomingByte);
        if( read == 'M')
            mute();
        else if (read == 'O')
            on_air();
    }
}