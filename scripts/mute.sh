#!/bin/bash
# Configuration

# Serial: the path to the serial port.
serial=/dev/ttyACM0


# device_name is the name of the device. This is the word between [] in /proc/asound/cards
device_name="Device"

# now, it's time to find the control that manage the capture device. This one is easy. or not.
# Look at the output of 
# amixer -c `cat /proc/asound/cards | grep "\[DEVICE_NAME" | awk '{print $1}'` scontrols
# Where DEVICE_NAME is the name of the device above.
# If you're lucky, your card will have a nice name for the capture control, like 'Mic' or 'Capture'.
# Otherwise, you will probably have to test them all to find the right one. Good Luck :)
scontrol_name='Mic'



# Let's do stuff.
card_id=`cat /proc/asound/cards | grep "\[$device_name" | awk '{print $1}'`

output=`amixer -c $card_id set $scontrol_name toggle | tail -n 1`

on_pat='Capture.*\[on\]'
off_pat='Capture.*\[off\]'

if [[ $output =~ $on_pat ]]; then
    echo "O" > $serial
    notify-send --icon /usr/share/icons/gnome/16x16/devices/audio-input-microphone.png "Mic turned on"
else
    if [[ $output =~ $off_pat ]]; then
        echo "M" > $serial
        notify-send --icon /usr/share/icons/gnome/16x16/devices/audio-input-microphone.png "Mic turned off"
    else
        notify-send -u critical "Mic Toggle failed: " "$output"
    fi
fi