EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L crafting_labs:Arduino_micro U2
U 1 1 6044F0EA
P 5950 2450
F 0 "U2" H 5975 3225 50  0000 C CNN
F 1 "Arduino_micro" H 5975 3134 50  0000 C CNN
F 2 "crafting-labs:arduino_micro" H 6050 1950 50  0001 C CNN
F 3 "" H 6050 1950 50  0001 C CNN
	1    5950 2450
	1    0    0    -1  
$EndComp
$Comp
L crafting_labs:display_128x32_i2c U1
U 1 1 6044F24D
P 4400 2400
F 0 "U1" H 5294 1985 50  0000 C CNN
F 1 "display_128x32_i2c" H 5294 2076 50  0000 C CNN
F 2 "crafting-labs:display_128x32_i2c" H 6150 2200 50  0001 C CNN
F 3 "" H 6150 2200 50  0001 C CNN
	1    4400 2400
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 6044FCD2
P 5950 1350
F 0 "SW2" H 5950 1635 50  0000 C CNN
F 1 "Reset" H 5950 1544 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_TL3305A" H 5950 1550 50  0001 C CNN
F 3 "~" H 5950 1550 50  0001 C CNN
	1    5950 1350
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 6044FE32
P 5200 2700
F 0 "SW1" H 5200 2985 50  0000 C CNN
F 1 "SW_Push" H 5200 2894 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_TL3305A" H 5200 2900 50  0001 C CNN
F 3 "~" H 5200 2900 50  0001 C CNN
	1    5200 2700
	0    -1   -1   0   
$EndComp
Text GLabel 6850 2000 2    50   Input ~ 0
GND
Text GLabel 6850 2200 2    50   Input ~ 0
VCC
Text GLabel 6850 2100 2    50   Input ~ 0
RST
Text GLabel 4600 2450 2    50   Input ~ 0
SCK
Text GLabel 5200 2400 0    50   Input ~ 0
SCK
Text GLabel 5200 2300 0    50   Input ~ 0
SDA
Text GLabel 4600 2550 2    50   Input ~ 0
SDA
Wire Wire Line
	4400 2550 4600 2550
Wire Wire Line
	4400 2450 4600 2450
Wire Wire Line
	5200 2300 5500 2300
Wire Wire Line
	5500 2400 5200 2400
Wire Wire Line
	6450 2000 6850 2000
Wire Wire Line
	6850 2100 6450 2100
Wire Wire Line
	6450 2200 6850 2200
Text GLabel 5200 3000 0    50   Input ~ 0
GND
Wire Wire Line
	5500 2500 5200 2500
Wire Wire Line
	5200 2900 5200 3000
Text GLabel 4600 2250 2    50   Input ~ 0
GND
Text GLabel 4600 2350 2    50   Input ~ 0
VCC
Wire Wire Line
	4400 2350 4600 2350
Wire Wire Line
	4600 2250 4400 2250
Text GLabel 6300 1350 2    50   Input ~ 0
RST
Text GLabel 5600 1350 0    50   Input ~ 0
GND
Wire Wire Line
	5600 1350 5750 1350
Wire Wire Line
	6150 1350 6300 1350
NoConn ~ 6450 2300
NoConn ~ 6450 2400
NoConn ~ 6450 2500
NoConn ~ 6450 2600
NoConn ~ 6450 2700
NoConn ~ 6450 2800
NoConn ~ 6450 2900
NoConn ~ 6450 3000
NoConn ~ 5500 3000
NoConn ~ 5500 2900
NoConn ~ 5500 2800
NoConn ~ 5500 2700
NoConn ~ 5500 2600
NoConn ~ 5500 2000
NoConn ~ 5500 1900
NoConn ~ 6450 1900
$EndSCHEMATC
