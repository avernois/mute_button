# Mute button

![a button display 'on air'](images/2021.03.30-mute_button-on_air.800px.jpg)

A physical button to mute a microphone with a visual feedback.

## Main idea

When you push the button, a script is called on the computer to toggle the microphone.
That script also send the result of the operation to the button to give feedback on the status.

# Details

## the button side

The button is controlled by an Arduino micro connected via USB to the computer.
The arduino presents itself as a HID device, so it can emulate a keyboard.
When the button is pressed, the arduino send a combination of keys (currently Shift + GUI + F1).

The arduino also listen on the USB so it can receive feedback.

### pcb

Design for a pcb is to be found in [pcb](pcb/). 
They are made with [KiCad](https://kicad.org/) and require [my kicad library](https://gitlab.com/avernois/kicad-lib) for custom symbols and footprints.

### list of materials

* an arduino micro
* a i2c 128x32 oled display
* 2 momentary buttons
* 2 1x12 pins header (for the arduino)

## the computer side

The combination of keys send by the arduino has to be configured to call the `scripts/mute.sh`.
The script uses `amixer` to toggle the correct (if you configured it well :). It then uses the output of that command to figure out the current state of the device and send it back to the arduino (so it set the display accordingly).

## configuration

* script:
  edit scripts/mute.sh with to set the information about your sound card and the serial port used by the arduino.

* system:
  configure a keyboard shortcut in your system so that <LEFT-SHIFT>+<GUI>+<F1> launches the script.

# FAQ

## What are the requirements?

It works on my ubuntu 18.04.
I'm pretty sure you could make it works quite easily on any linux as long as the sound is managed with ALSA, `amixer` is installed and you have enough rights to write on the serial port.
Also the script uses `notify-send` which might be a Gnome thing.

With a rewrite of the script, I'm confident it could be done for Apple computers.

I have no idea about Windows.

## Will it works for me?

No idea. Let me know if it does. Or does not. I might help (but maybe I won't).

## Why configure the card via its name and not directly to card id (as it is what is used by amixer)

Because, one (a younger me) might think that the card id is a reliable thing and the same card will always have the same id. One would eventually be proven wrong (and speak out loud while thinking their mic was mute. It was not.).

Note: one could configure alsa so a card will always have the same id. But I am not that one.

## Why a 128x32 oled display?

It was laying around. Also, I saw the idea from (https://github.com/FreeYourStream/freedeck-hardware), and really liked it.

## Where are the electronics schema?

Design for a pcb is to be found in [pcb](pcb/). 
They are made with [KiCad](https://kicad.org/) and require [my kicad library](https://gitlab.com/avernois/kicad-lib) for custom symbols and footprints.


## What's next?
* ~~a pcb~~
* ~~replace the nice but expensive teensy by a cheap arduino pro micro~~
* ~~make a small version (with smd button and components directly soldered to the pcb(without header))~~
* I'm open to suggestion.

# Licence

All code and design on this repository is placed under the Hippocratic 2.1 licence. See details in [LICENCE.md](LICENCE.md).

You can read more about that licence on [the website firstdonoharm.dev](https://firstdonoharm.dev/).